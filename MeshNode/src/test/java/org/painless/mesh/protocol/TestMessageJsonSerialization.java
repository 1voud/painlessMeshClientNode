package org.painless.mesh.protocol;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class TestMessageJsonSerialization {
	
	@Test
	public void testSyncReplyMessage() {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.enableDefaultTyping();
			int dest = 39485;
			int from = 1;
			SyncMessage sync = new SyncReplyMessage(dest, from, null);
			
			String json = mapper.writeValueAsString(sync);
			sync.addSub(new Node(1000));
			json = mapper.writeValueAsString(sync);
			Node node = new Node(1001);
			node.addSub(new Node(2000));
			sync.addSub(node);
			json = mapper.writeValueAsString(sync);
			System.out.println(json);

			Object o = mapper.readValue(json, BaseMessage.class);
			
			assertTrue(o instanceof SyncReplyMessage);
			SyncReplyMessage sMsg = (SyncReplyMessage)o; 
			assertEquals(sMsg.getType(), MessageType.NODE_SYNC_REPLY);
			assertEquals(sMsg.getSubs().length, 2);
			assertEquals(sMsg.getDest(), dest);
			assertEquals(sMsg.getFrom(), from);
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}
	@Test
	public void testSyncRequestMessage() {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.enableDefaultTyping();
			int dest = 39485;
			int from = 1;
			SyncMessage sync = new SyncRequestMessage(dest, from, null);
			
			String json = mapper.writeValueAsString(sync);
			sync.addSub(new Node(1000));
			json = mapper.writeValueAsString(sync);
			sync.addSub(new Node(1001));
			json = mapper.writeValueAsString(sync);
			System.out.println(json);
		
			Object o = mapper.readValue(json, BaseMessage.class);
			
			assertTrue(o instanceof SyncRequestMessage);
			SyncRequestMessage sMsg = (SyncRequestMessage)o; 
			assertEquals(sMsg.getType(), MessageType.NODE_SYNC_REQUEST);
			assertEquals(sMsg.getSubs().length, 2);
			assertEquals(sMsg.getDest(), dest);
			assertEquals(sMsg.getFrom(), from);
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}

}
