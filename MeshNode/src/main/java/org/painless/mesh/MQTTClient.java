package org.painless.mesh;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.painless.mesh.protocol.BaseMessage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MQTTClient implements MqttCallback {

	private static final Logger logger = Logger.getLogger(MQTTClient.class);

    private String broker = "tcp://localhost:1883";
    private MemoryPersistence persistence = new MemoryPersistence();
    private MqttClient mqttClient;
	private ObjectMapper mapper = new ObjectMapper();
	private Router router;
	
	private BlockingQueue<BaseMessage> queue = new LinkedBlockingQueue<>();

    public void connect(long clientId, Router router) {
        try {
        	this.router = router;
        	mqttClient = new MqttClient(broker, String.valueOf(clientId), persistence);
        	mqttClient.setCallback(this);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            mqttClient.connect(connOpts);
            mqttClient.subscribe("mesh/out/raw");
            new Thread() {
            	public void run() {
            		while (true) {
	                	try {
	                		BaseMessage message = queue.take();
	            			byte[] json = mapper.writeValueAsBytes(message);
	            			mqttClient.publish("mesh/in/raw", json, 2, false);
	            		} catch (MqttPersistenceException e) {
	                        logger.error(e);
	            		} catch (MqttException e) {
	                        logger.error(e);
	            		} catch (JsonProcessingException e) {
	                        logger.error(e);
	            		} catch (InterruptedException e) {
	                        logger.error(e);
						} catch (Exception e) {
	                        logger.error(e);

						}
            		}
            	};
            }.start();
        } catch(MqttException me) {
            logger.error("reason "+me.getReasonCode());
            logger.error("msg "+me.getMessage());
            logger.error("loc "+me.getLocalizedMessage());
            logger.error("cause "+me.getCause());
            logger.error("excep "+me);
            logger.error(me);
        }
	}
    
    public void publish(BaseMessage message) throws InterruptedException {
    	queue.put(message);
    }

    
	@Override
	public void connectionLost(Throwable cause) {
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		try {
			if ("mesh/out/raw".equals(topic)) {
				BaseMessage msg = mapper.readValue(message.getPayload(), BaseMessage.class);
				router.messageArrived(msg);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
	}
}
