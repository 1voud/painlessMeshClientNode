package org.painless.mesh;

import java.util.concurrent.BlockingQueue;

import org.painless.mesh.protocol.BaseMessage;

public interface MessageReceivedListener {
	
	void received(BaseMessage msg, 	BlockingQueue<BaseMessage> queue);
	void failed(long nodeId);
	void sendCompleted();
	
}
