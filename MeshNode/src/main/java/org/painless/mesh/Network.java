package org.painless.mesh;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import org.apache.log4j.Logger;

public class Network {

	private static final Logger logger = Logger.getLogger(Network.class);

	public NetworkInterface getInterface(String ifaceName) throws SocketException {
		InetAddress addr = null;
		Enumeration<NetworkInterface> n = NetworkInterface.getNetworkInterfaces();
		for (; n.hasMoreElements();) {
			NetworkInterface e = n.nextElement();
			Enumeration<InetAddress> a = e.getInetAddresses();
			for (; a.hasMoreElements();) {
				addr = a.nextElement();
				if (addr instanceof Inet4Address && e.getName().equals(ifaceName)) {
					return e;
				}
			}
		}
		return null;
	}
	
	public byte[] getMacAddress(String ifaceName) throws SocketException {
		return getInterface(ifaceName).getHardwareAddress();
	}

	public long getNodeId(String ifaceName) throws SocketException {
		byte[] mac = new Network().getMacAddress(ifaceName);
		long nodeId = Byte.toUnsignedLong(mac[2]);
		nodeId <<= 8;
		nodeId |= Byte.toUnsignedLong(mac[3]);
		nodeId <<= 8;
		nodeId |= Byte.toUnsignedLong(mac[4]);
		nodeId <<= 8;
		nodeId |= Byte.toUnsignedLong(mac[5]);
		String macString = "";
		for (byte b : mac) {
			macString += Integer.toHexString(Byte.toUnsignedInt(b)) + ":";
		}
		macString = macString.substring(0, macString.length()-1);
		logger.info("Using mac address: " + macString + " node-id: " + nodeId);
		return nodeId;
	}

	
}
