package org.painless.mesh;

import org.painless.mesh.protocol.BaseMessage;

public interface MessageListener {
	
	void messageArrived(BaseMessage message);
	
}
