package org.painless.mesh.protocol;

import java.util.Map;

public class TimeSyncMessage extends BaseMessage {

	private Map<String, Long> msg;
	
	public TimeSyncMessage() {
		super();
		setType(MessageType.TIME_SYNC);
	}

	public TimeSyncMessage(int dest, int from, Map<String, Long> msg) {
		super(dest, from);
		setType(MessageType.TIME_SYNC);
		this.msg = msg;
	}

	public Map<String, Long> getMsg() {
		return msg;
	}

	public void setMsg(Map<String, Long> msg) {
		this.msg = msg;
	}
	
}
