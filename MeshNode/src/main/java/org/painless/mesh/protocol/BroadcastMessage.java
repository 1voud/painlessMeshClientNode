package org.painless.mesh.protocol;

public class BroadcastMessage extends Message {

		public BroadcastMessage() {
		super();
		setType(MessageType.BROADCAST);
	}

	public BroadcastMessage(int dest, int from, String msg) {
		super(dest, from, msg);
		setType(MessageType.BROADCAST);
	}
}
