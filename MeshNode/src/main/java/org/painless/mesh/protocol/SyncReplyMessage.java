package org.painless.mesh.protocol;

public class SyncReplyMessage extends SyncMessage {

	public SyncReplyMessage() {
		setType(MessageType.NODE_SYNC_REPLY);
	}
	
	public SyncReplyMessage(int dest, int from, BaseMessage[] subs) {
		super(dest, from, subs);
		setType(MessageType.NODE_SYNC_REPLY);
	}
}
