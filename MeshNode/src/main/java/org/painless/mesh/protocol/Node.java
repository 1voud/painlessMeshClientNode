package org.painless.mesh.protocol;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Node {
	
	private long nodeId;
	private List<Node> subs = new ArrayList<Node>();
	
	public Node() {
		super();
	}

	public Node(long nodeId) {
		super();
		this.nodeId = nodeId;
	}
	
	public long getNodeId() {
		return nodeId;
	}
	public void setNodeId(long nodeId) {
		this.nodeId = nodeId;
	}
	public Node[] getSubs() {
		return subs.toArray(new Node[] {});
	}

	public void setSubs(Node[] subs) {
		this.subs = Arrays.asList(subs);
	}
	
	public void addSub(Node sub) {
		this.subs.add(sub);
	}

}
