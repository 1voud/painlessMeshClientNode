package org.painless.mesh.protocol;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
public enum MessageType {
	
    UNSET(0),
    UNKNOWN_1(1),
    UNKNOWN_2(2),
    TIME_DELAY(3),
    TIME_SYNC(4),
    NODE_SYNC_REQUEST(5),
    NODE_SYNC_REPLY(6),
    CONTROL(7),  //deprecated    
    BROADCAST(8),  
    SINGLE(9);   
	
	private int type;
	
	MessageType(int type) {
		this.type = type;
	}
	
	public int getType() {
		return this.type;
	}
}
