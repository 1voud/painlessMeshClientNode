package org.painless.mesh.protocol;

public class SyncRequestMessage extends SyncMessage {

	public SyncRequestMessage() {
		setType(MessageType.NODE_SYNC_REQUEST);
	}
	
	public SyncRequestMessage(int dest, int from, BaseMessage[] subs) {
		super(dest, from, subs);
		setType(MessageType.NODE_SYNC_REQUEST);
	}

}
