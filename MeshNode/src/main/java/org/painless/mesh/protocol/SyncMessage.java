package org.painless.mesh.protocol;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class SyncMessage extends BaseMessage {

	private List<Node> subs = new ArrayList<Node>();
	
	public SyncMessage() {
		setType(MessageType.NODE_SYNC_REQUEST);
	}
	
	public SyncMessage(int dest, int from, BaseMessage[] subs) {
		super(dest, from);
		setType(MessageType.NODE_SYNC_REQUEST);
	}

	public Node[] getSubs() {
		return subs.toArray(new Node[] {});
	}

	public void setSubs(Node[] subs) {
		this.subs = Arrays.asList(subs);
	}
	
	public void addSub(Node sub) {
		this.subs.add(sub);
	}

	public void removeSub(Node sub) {
		this.subs.remove(sub);
	}
		
}
