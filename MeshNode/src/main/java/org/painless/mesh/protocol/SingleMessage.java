package org.painless.mesh.protocol;

public class SingleMessage extends Message {

	public SingleMessage() {
		super();
		setType(MessageType.SINGLE);
	}

	public SingleMessage(long dest, long from, String msg) {
		super(dest, from, msg);
		setType(MessageType.SINGLE);
	}
	
}
