package org.painless.mesh.protocol;

public class TimeDelayMessage extends Message {

	public TimeDelayMessage() {
		super();
		setType(MessageType.TIME_DELAY);
	}

	public TimeDelayMessage(int dest, int from, String msg) {
		super(dest, from, msg);
		setType(MessageType.TIME_DELAY);
	}
	
}
