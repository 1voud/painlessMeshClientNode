package org.painless.mesh.protocol;

public abstract class Message extends BaseMessage {

	private String msg;

	public Message() {
		super();
	}

	public Message(long dest, long from, String msg) {
		super(dest, from);
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
