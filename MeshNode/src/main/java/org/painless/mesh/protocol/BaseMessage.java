package org.painless.mesh.protocol;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
	    use = JsonTypeInfo.Id.NAME,
	    include = JsonTypeInfo.As.EXISTING_PROPERTY,
	    property = "type")
@JsonSubTypes({
    @Type(value = org.painless.mesh.protocol.BaseMessage.class, name = "0"),
    @Type(value = org.painless.mesh.protocol.TimeDelayMessage.class, name = "3"),
    @Type(value = org.painless.mesh.protocol.TimeSyncMessage.class, name = "4"),
    @Type(value = org.painless.mesh.protocol.SyncRequestMessage.class, name = "5"),
    @Type(value = org.painless.mesh.protocol.SyncReplyMessage.class, name = "6"),
    @Type(value = org.painless.mesh.protocol.BroadcastMessage.class, name = "8"),
    @Type(value = org.painless.mesh.protocol.SingleMessage.class, name = "9")
    })

public class BaseMessage {
	
	private long dest;
    private long from;
    private MessageType type = MessageType.UNSET;
    
	public BaseMessage() {
	}
	
	public BaseMessage(long dest, long from) {
		super();
		this.dest = dest;
		this.from = from;
	}
	public MessageType getType() {
		return type;
	}

	public void setType(MessageType type) {
		this.type = type;
	}

	public long getDest() {
		return dest;
	}
	public void setDest(long dest) {
		this.dest = dest;
	}
	public long getFrom() {
		return from;
	}
	public void setFrom(long from) {
		this.from = from;
	}
    
}
