package org.painless.mesh;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class MeshAP {
	
	private static final Logger logger = Logger.getLogger(MeshAP.class);

	public static long MY_NODE_ID;
	public final static int MESH_PORT = 5555;
	
	private MQTTClient mqttClient = new MQTTClient();
	private Router router = new Router(mqttClient);

	public static void main(String[] args) {
		BasicConfigurator.configure();
		try {
			MY_NODE_ID = new Network().getNodeId(args[0]);
	    	new MeshAP().start();
		} catch (Throwable e) {
            logger.error(e);
		}
	}
	
	public void start() throws IOException {
		ServerSocket serverSocket  = null;
	    try {
	    	mqttClient.connect(MY_NODE_ID, router);
			serverSocket = new ServerSocket(MESH_PORT);
	    	while (true) {
				Socket clientSocket = serverSocket.accept();
	    		logger.info("New client");
				new ClientHandler(clientSocket, router);
			}			
		} catch (IOException e) {
            logger.error(e);
		} finally {
			serverSocket.close();
		}
	}

}
