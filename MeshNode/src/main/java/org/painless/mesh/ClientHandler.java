package org.painless.mesh;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.painless.mesh.protocol.BaseMessage;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ClientHandler implements MessageListener {
	
	private static final Logger logger = Logger.getLogger(ClientHandler.class);
	
	private Thread receiverThread;
	private Thread senderThread;
	private BlockingQueue<BaseMessage> queue = new LinkedBlockingQueue<>();
	private Router router;
	private MessageReceivedListener receiverListener;
	private Socket clientSocket;
	private long lastNode = -1;
	
	public ClientHandler(Socket clientSocket, Router router) throws IOException {
		this.clientSocket = clientSocket;
		clientSocket.setSoTimeout(30000);
		this.router = router;
		this.router = router;
		router.registerListener(this);
		receiverThread = new Thread(new Receiver());
		senderThread = new Thread(new Sender());
		receiverThread.start();
		senderThread.start();
		receiverListener = router;
	}

	@Override
	public void messageArrived(BaseMessage message) {
		if (message.getFrom() != lastNode) {
			logger.debug("Route: dest - " + message.getDest() + " - " + message.getFrom());
			queue.offer(message);
		}
	}

	public void failed(long nodeId) {
		receiverListener.failed(nodeId);
		router.deRegisterListener(this);
		try {
			senderThread.interrupt();
		} catch (Throwable th) {
            logger.error(th);
		}
		try {
			receiverThread.interrupt();
		} catch (Throwable th) {
            logger.error(th);
		}
		try {
			clientSocket.close();
		} catch (Throwable th) {
            logger.error(th);
		}
	}

	public void received(BaseMessage msg, BlockingQueue<BaseMessage> queue) {
		receiverListener.received(msg, queue);
	}

	public void sendCompleted() {
		receiverListener.sendCompleted();
	}
	
	class Receiver implements Runnable {
		
		private InputStream in;
		private ObjectMapper mapper = new ObjectMapper();
		
		public Receiver() throws IOException {
			this.in = clientSocket.getInputStream();
		}
		
		@Override
		public void run() {
			StringBuffer buffer = new StringBuffer();
			byte[] b = new byte[1];
			int depth = 0;
			while (true) {
				try {
					int c = in.read(b);
					if (c < 0) {
						ClientHandler.this.failed(lastNode);
						return;
					}
					buffer.append(new String(b));
					if (b[0] == '{') {
						depth++;
					} else if (b[0] == '}') {
						depth--;
					}
					if (depth == 0) {
						logger.info("<-- " + buffer.toString());
						BaseMessage msg = mapper.readValue(buffer.toString(), BaseMessage.class);
						if (lastNode < 0) {
							logger.info("Thread for: " + msg.getFrom());
							lastNode = msg.getFrom();
							receiverThread.setName("RECEIVE-"+ lastNode);
							senderThread.setName("SEND-"+ lastNode);
						}
						ClientHandler.this.received(msg, queue);
						buffer = new StringBuffer();
					}
				} catch (IOException e) {
		            logger.error(e);
					ClientHandler.this.failed(lastNode);
					return;
				}
			}
		}		
	}
	
	class Sender implements Runnable {
		
		private OutputStream out;
		private ObjectMapper mapper = new ObjectMapper();
		
		public Sender() throws IOException {
			this.out = new BufferedOutputStream(clientSocket.getOutputStream());
		}

		public void run() {
			while (true) {
				try {
					BaseMessage msg = queue.poll(30, TimeUnit.SECONDS);
					if (msg == null) {
						ClientHandler.this.failed(lastNode);
						return;
					}
					String json = mapper.writeValueAsString(msg);
					logger.info("--> "+ json);
					out.write(json.getBytes());
					out.flush();
					ClientHandler.this.sendCompleted();
				} catch (IOException e) {
					ClientHandler.this.failed(lastNode);
		            logger.error(e);
					ClientHandler.this.failed(lastNode);
					return;
				} catch (InterruptedException e) {
					ClientHandler.this.failed(lastNode);
		            logger.error(e);
					ClientHandler.this.failed(lastNode);
					return;
				}					
			}
		}
	}

}
