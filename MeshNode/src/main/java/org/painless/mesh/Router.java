package org.painless.mesh;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.painless.mesh.protocol.BaseMessage;
import org.painless.mesh.protocol.MessageType;
import org.painless.mesh.protocol.Node;
import org.painless.mesh.protocol.SyncReplyMessage;
import org.painless.mesh.protocol.SyncRequestMessage;
import org.painless.mesh.protocol.TimeSyncMessage;

public class Router implements MessageListener, MessageReceivedListener {

	private static final Logger logger = Logger.getLogger(Router.class);
	
	private List<MessageListener> listeners = Collections.synchronizedList(new ArrayList<MessageListener>());
	private MQTTClient mqttClient = new MQTTClient();
	private long timeOffset = System.nanoTime();
	private Map<Long, Node[]> nodes = new ConcurrentHashMap<>();

    public Router(MQTTClient mqttClient) {
		super();
		this.mqttClient = mqttClient;
	}
	public void registerListener(MessageListener listener) {
    	listeners.add(listener);
    }
   
    public void deRegisterListener(MessageListener listener) {
    	listeners.remove(listener);
    }
    
	@Override
	public void messageArrived(BaseMessage message) {
		for (MessageListener messageListener : listeners) {
			messageListener.messageArrived(message);
		}
	}
	@Override
	public void received(BaseMessage msg, BlockingQueue<BaseMessage> queue) {
//		if (msg.getFrom() == MY_NODE_ID) {
//			System.out.println("Van mijzelf");
//		}
		long receivedTime = (System.nanoTime())-timeOffset;
		try {
			mqttClient.publish(msg);
			if (msg.getType() == MessageType.TIME_DELAY) {
				msg.setDest(msg.getFrom());
				msg.setFrom(MeshAP.MY_NODE_ID);
				queue.offer(msg);
			} else if (msg.getType() == MessageType.TIME_SYNC) {
				TimeSyncMessage tsMsg = (TimeSyncMessage)msg;
				if (tsMsg.getMsg().get("type") != 2) {
				
					if (tsMsg.getMsg().get("type") == 0) {
						tsMsg.setDest(msg.getFrom());
						msg.setFrom(MeshAP.MY_NODE_ID);
						tsMsg.getMsg().put("type", 1l);
						tsMsg.getMsg().put("t0", (System.nanoTime()-timeOffset)/1000);
						queue.put(tsMsg);
					} else if (tsMsg.getMsg().get("type") == 1) {
						long t0 = tsMsg.getMsg().get("t0");						
						tsMsg.setDest(msg.getFrom());
						msg.setFrom(MeshAP.MY_NODE_ID);
						tsMsg.getMsg().put("type", 2l);
						tsMsg.getMsg().put("t0", t0);
						tsMsg.getMsg().put("t1", receivedTime/1000);
						tsMsg.getMsg().put("t2", (System.nanoTime()-timeOffset)/1000);
	
						queue.put(tsMsg);
					}
				} else {
					timeOffset += ((tsMsg.getMsg().get("t1") - tsMsg.getMsg().get("t0"))/2 - (tsMsg.getMsg().get("t0")-receivedTime/1000)/2);					
				}
			} else if (msg.getType() == MessageType.NODE_SYNC_REQUEST) {
				SyncReplyMessage srMsg = new SyncReplyMessage();
				srMsg.setDest(msg.getFrom());
				msg.setFrom(MeshAP.MY_NODE_ID);
				nodes.put(msg.getFrom(), ((SyncRequestMessage)msg).getSubs());

				for (long nodeId : nodes.keySet()) {
					if (nodeId != srMsg.getDest() && nodeId != MeshAP.MY_NODE_ID) {
						Node node = new Node();
						node.setNodeId(nodeId);
						node.setSubs(nodes.get(nodeId));
						srMsg.addSub(node);
					}
				}
				logger.info("Current Nodes: " + nodes.size());
				queue.put(srMsg);
			} else if (msg.getType() == MessageType.SINGLE || msg.getType() == MessageType.BROADCAST) {
				logger.info("Route: dest - " + msg.getDest() + " - " + msg.getFrom());
				
				this.messageArrived(msg);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void failed(long nodeId) {
		nodes.remove(nodeId);
		logger.info("Read failed " + nodeId + " left");
	}

	@Override
	public void sendCompleted() {
	}	
}
